class CharCountableString implements CountableString {

    private final String stringValue;

    CharCountableString(final String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public boolean contains(final int qty, final char character) {
        return
            contains(stringValue.toCharArray(), qty, character);
    }

    private boolean contains(
        final char[] chars,
        final int qty,
        final char character
    ) {
        int count = 0;
        for (int i = 0; i < chars.length && count != (qty + 1); i++) {
            if (chars[i] == character) {
                count++;
            }
        }

        return (count == qty);
    }
}
