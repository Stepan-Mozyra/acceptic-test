public class Application {

    public static void main(final String[] args) {
        System.out.println(
            new CharCountableString("Hello World!")
                .contains(3, 'l')
        );
        System.out.println(
            new CharCountableString("Hello World!")
                .contains(2, 'e')
        );
        System.out.println(
            new CharCountableString("Hello-hello World!")
                .contains(2, 'e')
        );
        System.out.println(
            new CharCountableString("Hello-hello-hello World!")
                .contains(2, 'e')
        );
        System.out.println(
            new CharCountableString("Hello-hello-hello-hello World!")
                .contains(2, 'e')
        );

        System.out.println();

        System.out.println(
            new CharacterCountableString("Hello World!")
                .contains(3, 'l')
        );
        System.out.println(
            new CharacterCountableString("Hello World!")
                .contains(2, 'e')
        );
    }
}
