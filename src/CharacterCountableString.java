import java.util.Arrays;
import java.util.function.ToIntFunction;

class CharacterCountableString implements CountableString {

    private final String stringValue;

    CharacterCountableString(final String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public boolean contains(final int qty, final char character) {
        final char[] chars = stringValue.toCharArray();
        final Character[] characters = new Character[chars.length];

        for (int i = 0; i < chars.length; i++) {
            characters[i] = chars[i];
        }

        return
            count(
                characters,
                (nextCharacter) -> nextCharacter == character ? 1 : 0
            ) == qty;
    }

    private int count(
        final Character[] characters,
        final ToIntFunction<Character> ifCharacterCounts
    ) {
        return
            Arrays.stream(characters)
                .mapToInt(ifCharacterCounts)
                .sum();
    }
}
