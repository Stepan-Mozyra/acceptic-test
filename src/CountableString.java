public interface CountableString {

    boolean contains(int qty, char character);

}
